import { Component, OnInit } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  slides = [1,2,3,4,5];
  imgs=['https://i.pinimg.com/474x/78/7a/e5/787ae5e6b24b917509d2680cfd421181.jpg','https://i.pinimg.com/originals/b6/97/b1/b697b10daa507b42b3f2edf051d5cfa5.jpg','https://i.pinimg.com/474x/78/7a/e5/787ae5e6b24b917509d2680cfd421181.jpg','https://i.pinimg.com/originals/b6/97/b1/b697b10daa507b42b3f2edf051d5cfa5.jpg','https://i.pinimg.com/474x/78/7a/e5/787ae5e6b24b917509d2680cfd421181.jpg','https://i.pinimg.com/originals/b6/97/b1/b697b10daa507b42b3f2edf051d5cfa5.jpg'];
  constructor() { }
  
  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };

  

  ngOnInit(): void {
  }

}
