import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.scss']
})
export class MycartComponent implements OnInit {

  constructor(private shared2:SharedService) { }
  public food1:any[]=[];
  public food2:any[]=[];
  ngOnInit(): void {
    this.shared2.on<any>().subscribe(
      data=>{
        this.food1.push(data);
      }
    )
    this.food2=this.shared2.foods;
  }


}
