import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public food:any[]=[];
  public items:any[]=[];
  constructor(private shared1:SharedService) { }
  
  ngOnInit(): void {
    this.food=this.shared1.foodItems;
    for (let key of Object.keys(this.food[0])) {
      this.items.push(key);
   }
  }
  cart(i:any)
  {
    this.shared1.emit<any>(i);
    this.shared1.foods.push(i);
  }

}
